import { FastifyPluginAsync } from "fastify";
import webpush from "web-push";

const vapidKeys = {
  publicKey:
    "BLbPwNboO-5hJKXeiI9Z_eCOMKuPCwhejJrsfOwz1MAzwIZMZxltBkARJ_n9X8a2gAE1uojmE2Z12r01W9cEMX8",
  privateKey: "jzxUEGHXZXFZ40ydqXz5FAq0aAhbzPumUdQJTnAqf6k",
};

const listSubscription: webpush.PushSubscription[] = [];
const noti: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  webpush.setVapidDetails("mailto:", vapidKeys.publicKey, vapidKeys.privateKey);

  fastify.get("/", async function (request, reply) {
    return vapidKeys;
  });

  fastify.post("/subscribe", async (request, reply) => {
    const subscription = request.body as webpush.PushSubscription;

    // check if subscription already exists
    const existingSubscription = listSubscription.find((sub) => {
      return sub.endpoint === subscription.endpoint;
    });

    if (!existingSubscription) {
      listSubscription.push(subscription);
    }

    reply.code(201).send({
      status: "ok",
    });
  });

  fastify.post("/unsubscribe", async (request, reply) => {
    const { endpoint } = request.body as { endpoint: string };
    if (!endpoint) {
      reply.code(400).send({
        status: "error",
        message: "endpoint is required",
      });
    }

    const index = listSubscription.findIndex((sub) => {
      return sub.endpoint === endpoint;
    });

    if (index !== -1) {
      listSubscription.splice(index, 1);
    }

    reply.code(201).send({
      status: "ok",
    });
  });

  fastify.post("/push", async (request, reply) => {
    const { title, content } = request.body as {
      title: string;
      content: string;
    };
    console.log(title, content);
    const playload = JSON.stringify({
      title: title ?? "title",
      body: content ?? "body",
      icon: "https://picsum.photos/536/354",
      image: "https://picsum.photos/536/354",
    });
    const options = {
      TTL: 3600,
    };
    listSubscription.forEach((sub) => {
      webpush
        .sendNotification(sub, playload, options)
        .catch((err) => console.error(err));
    });
    reply.code(200).send(listSubscription);
  });
};

export default noti;
